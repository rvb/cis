#include "color_set.hpp"
#include "interval_graph.hpp"
#include "dynprog.hpp"
#include "SubsetIterator.hpp"
#include <unordered_map>
#include <vector>
#include <limits>

color_set recoded;

struct LUT_static {
  size_t sets, cliques;
  unsigned int *table;
  
  LUT_static(size_t c, size_t s) : sets(s), cliques(c) {
    cout << "# want table for " << c << " cliques and " <<
      sets << " color sets:" << endl;
    cout << "+ memory MB: " <<
      (size_t)c*(size_t)s*sizeof(color_set::repType)/1024.0/1024.0 << endl;

    // check for overflow of c*s
    if(numeric_limits<size_t>::max() / cliques < sets) throw bad_alloc();
    table = new unsigned int[cliques*sets];
  }
  
  ~LUT_static() { delete[] table; }
  
  inline void clear(const size_t c) {}

  inline void set(const size_t c, const color_set &C, const unsigned int value) {
    table[(c%cliques)*sets+C.rep] = value;
  }
  
  inline unsigned int get(const size_t c, const color_set &C) {
    auto value = table[(c%cliques)*sets+C.rep];
    return value;
  }
};

struct LUT_dynamic {
  size_t cliques;
  vector<unordered_map<color_set::repType, unsigned int> > table;

  LUT_dynamic(size_t c, size_t s) : cliques(c), table(c) {}
  
  inline void set(const size_t c, const color_set &C, const unsigned int value) {
    table[c%cliques][C.rep] = value;
  }

  inline void clear(const size_t c) { table[c%cliques].clear(); }
  
  inline unsigned int get(const size_t c, const color_set &C) {
    return table[c%cliques].at(C.rep);
  }
};

struct cis_solver {
  const interval_graph &vlist;
  pair<unsigned int, unsigned int> cinfo;
  color_set all_colors;
  int maxweight;
  vector<color_set> relevant_right, relevant_left;

  inline color_set active_colors(size_t c) {
    return relevant_right[c] & relevant_left[c];
  }

  inline color_set dead_left(size_t c) {
    return all_colors - relevant_left[c];
  }

  cis_solver(const interval_graph &v) :
    vlist(v), cinfo(clique_info(vlist)),
    maxweight(0),
    relevant_right(cinfo.first + 2), relevant_left(cinfo.first + 2)
  {
    for(const auto &v : vlist) {
      all_colors |= v.c;
      maxweight = (v.w>maxweight?v.w:maxweight);
    }
    

    cout << "# Colors " << all_colors.size() << endl;
    cout << "# cliques " << cinfo.first << endl;
    cout << "# max weight " << maxweight << endl;
    cout << "+ avg. clique size : " << vlist.size() / (float)cinfo.first << endl;
    
    vector<color_set> tmp(cinfo.first + 2);
    
    for(auto i = vlist.rbegin(); i != vlist.rend(); ++i)
      tmp[i->start] = tmp[i->start] | i->c;

    relevant_right[0] = all_colors;
    for(unsigned int cur_clique = cinfo.first; cur_clique > 0; --cur_clique)
      relevant_right[cur_clique] = relevant_right[cur_clique + 1] | tmp[cur_clique];

    relevant_left[0] = color_set();
    for(unsigned int cur_clique = 1; cur_clique <= cinfo.first + 1; ++cur_clique)
      relevant_left[cur_clique] = relevant_left[cur_clique - 1]
	| tmp[cur_clique];
  }

  template<class L>
  unsigned int solve() {
    SubsetIterator<color_set> end;
    L lut(cinfo.second + 1, all_colors.rep + 1);

    lut.set(cinfo.first + 1, color_set(), 0);
    
    unsigned int oldstart = 0;
    for(auto i = vlist.rbegin(); i != vlist.rend(); ++i) {
      for(SubsetIterator<color_set> subsets(active_colors(i->start));
	  subsets != end; ++subsets) {
	if(i->start != oldstart) lut.set(i->start, *subsets, 0);
	unsigned int one = lut.get(i->start + 1, (*subsets | dead_left(i->start)) & active_colors(i->start + 1));
	unsigned int two = 0;
	if((i->c - *subsets).empty()) 
	  two = i->w + lut.get(i->end, (((*subsets - i->c) | dead_left(i->start)) & active_colors(i->end)));
	unsigned int three = lut.get(i->start, *subsets);
	if(two > one) one = two;
	if(one > three)
	  lut.set(i->start, *subsets, one);
      }
      oldstart = i->start;
    }
    return lut.get(1, active_colors(1));
  }
};

unsigned int cis_size(const interval_graph &vlist) {
  cis_solver solver(vlist);
  try { return solver.solve<LUT_static>(); }
  catch (bad_alloc &ba) {
    cout << "# not enough space for lookup table. Using slower hash table." << endl;
    if(getenv("MCIS_NO_DYNPROG_FALLBACK")) throw bad_alloc();
    return solver.solve<LUT_dynamic>();
  }
}
