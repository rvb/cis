#ifndef SEARCH_TREE_OMEGA_HPP
#define SEARCH_TREE_OMEGA_HPP

#include "interval_graph.hpp"
#include <list>

using namespace std;

unsigned int search_tree_omega(const interval_graph &G, unsigned int k);
unsigned int greedy(const interval_graph &G);

#endif
