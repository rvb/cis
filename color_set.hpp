#ifndef _COLOR_SET_HPP
#define _COLOR_SET_HPP

#include "EfficientSet.hpp"

typedef int color;
typedef EfficientSet<color, uint32_t> color_set;

#endif
