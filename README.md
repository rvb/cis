Maximum-profit subset of jobs mutually not conflicting in time or resources
===========================================================================

This program optimally solves the following NP-hard problem in O(2^_r_
_n_) time:

**Input:** A set of _n_ jobs, each characterized by a time interval used for processing, a profit, and a subset of _r_ resources consumed by it (resources are not renewable).   
**Output:** A maximum-profit subset of jobs that can be processed without conflicting in time or resources.

This is a so-called _fixed-parameter linear time_ algorithm, whose
running time depends linearly on the input size and exponentially only
on the smaller number of resources.

The problem easily models availability of multiple processing machines
and the Job Interval Selection problem, where one has _r_ jobs and, for
each job, a set of possible execution intervals with various profits
(_n_ intervals in total).  Details and example applications are
discussed in

- [René van Bevern. Fixed-Parameter Linear-Time Algorithms for NP-hard
  Graph and Hypergraph Problems Arising in Industrial Applications.
  Volume 1 in Foundations of Computing, Universitätsverlag der TU
  Berlin, 2014, ISBN
  978-3-7983-2705-4.](http://dx.doi.org/10.14279/depositonce-4131)

Experiments have shown that this fixed-parameter linear time algorithm
efficiently (in under five minutes) solves instances with hundreds of
thousands of jobs and up to 15 resources.  The theoretical and
experimental results also appeared in _Journal of Scheduling_:

- [René van Bevern, Matthias Mnich, Rolf Niedermeier, and Mathias
  Weller. Interval scheduling and colorful independent sets. _Journal of
  Scheduling_, 18(5):449–469,
  2015.](http://dx.doi.org/10.1007/s10951-014-0398-5)

Requirements / Installation
---------------------------

You can download the solver by downloading a [Zip
archive](https://gitlab.com/rvb/cis/repository/archive.zip?ref=master)
or [tar.gz
archive](https://gitlab.com/rvb/cis/repository/archive.tar.gz?ref=master)
or by cloning this git repository

```
git clone https://gitlab.com/rvb/cis.git
```

If necessary, unpack the archive.  If the [CMake](https://cmake.org/)
build system and the GNU C++ compiler are available, compiling should
work as follows.  In the directory that this `README.md` file is in,
execute:

```
cmake .
make
```

The result should be a `cis` executable, which can be used as follows.

Usage
-----
To solve the problem on a random interval graph with _n_ intervals, maximum
compactness _m_, with at most _c_ resources and a maximum profit _w_, use

```
./cis -i i -m m -c c -w w
```

For example

```
./cis -p -i 10000 -m 100 -c 15 -w 10
```

The option `-p` prints the generated interval graph in the program
specific format.

To solve the problem on an interval graph in a file `graph`, use

```
./cis -f graph
```

Use the option `-p` to see the maximally compactified interval graph.

Interval Graph format:
----------------------

The input format has on each line a right-open interval, which consists
of a start point, and end point, a weight, and a list of resources. For
example:

```
1 5 2 1 5 6 9 
1 3 6 8 9 10
```

We have here the interval [1,5) with weight 2 that uses resources 1, 5,
6, and 9. Moreover, there is an interval [1,3) with weight 6 that uses
resources 8, 9, and 10.

---
René van Bevern <rvb@nsu.ru>
