#include <list>
#include <vector>
#include <algorithm>
#include "preprocessing.hpp"
#include "interval_graph.hpp"

void horizontal_smash(interval_graph &vlist) {
  vector<pair<unsigned int, vertex*> > events;
  events.reserve(2*vlist.size());

  for(auto &v : vlist) {
    events.push_back(make_pair(v.start, &v));
    events.push_back(make_pair(v.end, &v));
  }

  stable_sort(events.begin(), events.end(), [](const pair<unsigned int, vertex*> &a,
		 const pair<unsigned int, vertex*> &b) {
		return(a.first < b.first);
	      });

  unsigned int previous_pos = 1;
  bool previous_type = false; // end
  for(auto &e : events)
    if(previous_type) { // previous was a start event
      if(e.first == e.second->start) e.second->start = previous_pos;
      else if(e.first == e.second->end) {
	e.second->end = previous_pos = previous_pos + 1;
	previous_type = false;
      } 
    } else if(!previous_type) { // previous was end event
      if(e.first == e.second->start) {
	e.second->start = previous_pos;
	previous_type = true;
      }
      else if(e.first == e.second->end)	e.second->end = previous_pos;
    }
}

void sort_starts(interval_graph &vlist) {
  sort(vlist.begin(), vlist.end(), [](const vertex &a, const vertex &b) {
      return(a.start < b.start);
    });
}
