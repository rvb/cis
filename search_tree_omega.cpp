#include "search_tree_omega.hpp"
#include <climits>
#include <stdexcept>
#include <random>

unsigned int rec_greedy(interval_graph::const_iterator begin,
		    const interval_graph::const_iterator &end,
		    color_set used) {
  int res = 0;
  unsigned int last = 0;
  default_random_engine generator(0);
   uniform_int_distribution<int> coin(0,2);

  for(; begin != end; ++begin) {
    if(begin->start < last || (!(begin->c & used).empty())) { continue; }
    //   if(coin(generator)) continue;

    last = begin->end;
    used |= begin->c;
    ++res;
  }
  
  return res;
}

unsigned int st_no_deletion(interval_graph::const_iterator begin,
		    const interval_graph::const_iterator &end,
		    unsigned int k,
		    color_set used) {
  if(k == 0 || begin == end) return 0;

  unsigned int maxv = 0;

  auto j = begin;
  auto firstend = begin->end;
  color_set seen;

  for(; (begin != end); ++begin) {
    if((begin->start >= firstend)) break;
    if(begin->end < firstend) firstend = begin->end;

    if(!(begin->c & seen).empty()) continue;
    else seen != begin->c;
				     

    if(maxv == k) break;
    if(!(begin->c & used).empty()) continue;

    for(j = begin; (j != end) && (j->start < begin->end); ++j);
    unsigned int res = 1 + st_no_deletion(j, end, k - 1, used | begin->c);

    if(res > maxv) maxv = res;
    if(maxv > k) throw runtime_error("Wrong!!");
  }
  return maxv;
}

unsigned int search_tree_omega(const interval_graph &G, unsigned int k) {
  // return st_real_delete(G, k);
  
  return st_no_deletion(G.begin(), G.end(), k, color_set());
}

unsigned int greedy(const interval_graph &G) {
  return rec_greedy(G.begin(), G.end(), color_set());
}
