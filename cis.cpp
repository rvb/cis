#include "interval_graph.hpp"
#include <ctime>
#include "dynprog.hpp"
#include "preprocessing.hpp"
#include <cassert>
#include <getopt.h>
#include <cstdlib>
#include <stdexcept>
#include <functional>

using namespace std;

void stats_time(const char* heading, const function<void ()> &func) {
  cout << "---- " << heading << " ----" << endl;
    clock_t t = clock();
    func();
    t = clock() - t;
    cout << "+ seconds : " << ((float)t)/CLOCKS_PER_SEC << endl;
}

int main (int argc, char *argv[]) {
  int c;
  bool p = false;
  int num = 0;
  int colors = 0;
  int m = -1;
  int w = 1;
  interval_graph vlist;

  cout << "------------------------------------------------------" << endl;
  cout << "Solver for Maximum Colorful Independent Set with Lists" << endl;
  cout << "(C) 2013-2016 René van Bevern <rvb@nsu.ru>" << endl;
  cout << endl;
  cout << "The program is distributed under the terms and" << endl;
  cout << "conditions of the GNU General Public Licence and comes" << endl;
  cout << "with absolutely NO WARRANTY" << endl;
  cout << "------------------------------------------------------" << endl;

  while((c=getopt(argc, argv, "f:i:c:pm:w:")) != -1)
    switch (c) {
    case 'p': p = true; break;
    case 'f': stats_time("Reading from file", [&] () {vlist = read_intervals(optarg); }); break;
    case 'i': 
      num = atoi(optarg);
      assert(num >= 0);
      break;
    case 'm':
      m = atoi(optarg);
      assert(m >= 0);
      break;
    case 'w':
      w = atoi(optarg);
      assert(w >= 1);
      break;
    case 'c':
      colors = atoi(optarg);
      assert(colors >= 0);
      break;
    default:
      cout << "Usage: " << argv[0] << "[-f <filename>] [-i <#intervals> -m <#max compactness> -c <#colors> -w <max weight>]" << endl
	   << "-p\tPrint loaded intervals" << endl
	   << "-f\tLoad graph from file" << endl
	   << "-i -m -c -w\tLoad random interval graph with given parameters" << endl;
      exit(EXIT_FAILURE);
    }

  if(vlist.empty()) {
    if(m<0) m = num;
    if(num > 0 && colors > 0)
      stats_time("Generating random graph", [&] () { vlist = random_intervals_nolength(num, colors, m, w); });
    else throw runtime_error("No interval graph loaded.");
  }

  stats_time("Sorting intervals", [&] () { sort_starts(vlist); });
  stats_time("Compactifying coordinates", [&] () { horizontal_smash(vlist); });
  if(p) print_intervals(vlist);
  stats_time("Dynamic Program", [&] ()
	     { cout << "+ MCIS " << cis_size(vlist) << endl; });
  exit(EXIT_SUCCESS);
}
