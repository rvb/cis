#ifndef PREPROCESSING_HPP
#define PREPROCESSING_HPP

#include "interval_graph.hpp"

using namespace std;

void horizontal_smash(interval_graph &intervals);
void sort_starts(interval_graph &intervals);

#endif

