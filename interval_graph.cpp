#include "interval_graph.hpp"
#include "color_set.hpp"
#include <iostream>
#include <cassert>
#include <sstream>
#include <fstream>
#include <random>
#include <chrono>
#include <climits>

using namespace std;

interval_graph read_intervals(const char *file) {
  ifstream input(file, ifstream::in);
  interval_graph vlist;
  string line;

  if(!input) throw ios_base::failure("Given interval graph does not exist");

  getline(input, line);
  while(input.good()) {
    if(line[0]!='#') {
      vertex v;
      color c;

      istringstream linestream(line);
      
      linestream >> v.start;
      linestream >> v.end;
      linestream >> v.w;
      
      while(linestream >> c)
	v.c.insert(c);

      assert(v.start < v.end);
      assert(v.w > 0);
      
      vlist.push_back(v);
    }
    getline(input, line);
  }
  
  return vlist;
}

pair<unsigned int, unsigned int> clique_info(const interval_graph &vlist) {
  unsigned int last_clique = 0;
  unsigned int max_length = 0;

  for(auto &v: vlist) {
    unsigned int clnum = v.end - 1;
    unsigned int clength = v.end - v.start;
    if(clnum > last_clique) last_clique = clnum;
    if(clength > max_length) max_length = clength;
  }

  return make_pair(last_clique, max_length);
}


void print_intervals(const interval_graph &vlist) {
  for(auto &v : vlist) {
    cout << v.start << " " << v.end << " " << v.w << " ";
    v.c.printSet();
    cout << endl;
  }
}

interval_graph random_intervals_nolength(int n, color c, int d, int w) {
  default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
  uniform_int_distribution<int> uni_int(1,d);
  uniform_int_distribution<color> uni_col(1,2);
  uniform_int_distribution<int> uni_weight(1,w);
  interval_graph vlist;
  vlist.reserve(n);

  for(int n_ = 1; n_ <= n; ++n_) {
    vertex v;
    int one = uni_int(generator);
    int two = one;
    for(; two == one; two = uni_int(generator) + 1);

    if(one > two) {v.start = two; v.end = one; }
    else {v.start = one; v.end = two; }
    v.w = uni_weight(generator);

    while(v.c.empty())
      for(int col=1; col <= c; ++col)
	if(uni_col(generator)==1) v.c|=color_set::singleton(col);
    

    
    vlist.push_back(v);
  }

  return vlist;
}


