#ifndef _INTERVAL_GRAPH_HPP
#define _INTERVAL_GRAPH_HPP

#include <vector>
#include "color_set.hpp"

using namespace std;

struct vertex {
  unsigned int start;
  unsigned int end;
  color_set c;
  int w;

  vertex(int s, int e) : start(s), end(e) {}
  vertex() : start(0), end(0) {}
};

typedef vector<vertex> interval_graph;

interval_graph read_intervals(const char *file);
void print_intervals(const interval_graph &vlist);
pair<unsigned int, unsigned int> clique_info(const interval_graph &vlist);
interval_graph random_intervals_nolength(int l, color c, int d, int w);

#endif


